export const CONNECTED = "CONNECTED";
export const USER_CONNECTED = "USER_CONNECTED";
export const USER_DISCONNECTED = "USER_DISCONNECTED";
export const NEW_MESSAGE = "NEW_MESSAGE";

export const websocketAction = data => ({...data});