import {CONNECTED, NEW_MESSAGE, USER_CONNECTED, USER_DISCONNECTED} from "../actions/chatActions";

export const initialState = {
  users: [],
  messages: [],
};

const chatReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONNECTED:
      return {...state, users: action.users, messages: action.messages.reverse()};
    case USER_CONNECTED:
      return {...state, users: [...state.users, action.user]};
    case USER_DISCONNECTED:
      return {...state, users: [...state.users.filter(user => user !== action.user)]};
    case NEW_MESSAGE:
      return {...state, messages: [...state.messages, action.newMessage]};
    default:
      return state;
  }
};

export default chatReducer;