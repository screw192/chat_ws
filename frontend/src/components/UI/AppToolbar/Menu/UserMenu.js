import React from 'react';
import {useDispatch} from "react-redux";
import {Button, Grid, Typography} from "@material-ui/core";

import {logoutUser} from "../../../../store/actions/usersActions";


const UserMenu = ({user}) => {
  const dispatch = useDispatch();

  return (
      <Grid container alignItems="center">
        <Grid item>
          <Typography variant="body1" display="inline">
            Welcome, {user.displayName}!
          </Typography>
        </Grid>
        <Grid item>
          <Button
              color="inherit"
              onClick={() => dispatch(logoutUser())}
          >
            Logout
          </Button>
        </Grid>
      </Grid>
  );
};

export default UserMenu;