import React from 'react';
import {AppBar, Grid, Toolbar, Typography} from "@material-ui/core";
import {useSelector} from "react-redux";

import UserMenu from "./Menu/UserMenu";
import AnonymousMenu from "./Menu/AnonymousMenu";


const AppToolbar = () => {
  const user = useSelector(state => state.users.user);

  return (
      <>
        <AppBar position="static">
          <Toolbar>
            <Grid container justify="space-between" alignItems="center">
              <Grid item>
                <Typography variant="h6">Le chatique</Typography>
              </Grid>
              <Grid item>
                {user ? (
                    <UserMenu user={user}/>
                ) : (
                    <AnonymousMenu/>
                )}
              </Grid>
            </Grid>
          </Toolbar>
        </AppBar>
      </>
  );
};

export default AppToolbar;