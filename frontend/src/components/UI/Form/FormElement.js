import React from 'react';
import {Grid, TextField} from "@material-ui/core";

const FormElement = ({error, ...props}) => {
  return (
      <Grid item xs>
        <TextField
            error={Boolean(error)}
            helperText={error}
            {...props}
        />
      </Grid>
  );
};

export default FormElement;