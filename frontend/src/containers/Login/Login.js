import React, {useState} from "react";
import {Link as RouterLink} from "react-router-dom";
import {useDispatch} from "react-redux";
import {Avatar, Button, Container, Grid, Link, makeStyles, Typography} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";

import FormElement from "../../components/UI/Form/FormElement";
import {loginUser} from "../../store/actions/usersActions";


const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  header: {
    marginBottom: theme.spacing(2)
  }
}));

const Login = () => {
  const classes = useStyles();
  const dispatch = useDispatch();

  const [user, setUser] = useState({
    email: "", password: "",
  });

  const inputChangeHandler = e => {
    const {name, value} = e.target;

    setUser(prev => ({...prev, [name]: value}));
  };

  const submitFormHandler = e => {
    e.preventDefault();

    dispatch(loginUser({...user}));
  };

  return (
      <Container component="section" maxWidth="xs">
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon/>
          </Avatar>
          <Typography component="h1" variant="h5" className={classes.header}>
            Login
          </Typography>
          <Grid container spacing={1} direction="column" component="form" onSubmit={submitFormHandler} noValidate>
            <FormElement
                label="Email"
                type="email"
                autoComplete="current-email"
                onChange={inputChangeHandler}
                name="email"
                value={user.email}
            />
            <FormElement
                label="Password"
                type="password"
                autoComplete="current-password"
                onChange={inputChangeHandler}
                name="password"
                value={user.password}
            />
            <Grid item xs>
              <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
              >
                Login
              </Button>
            </Grid>
            <Grid item container justify="flex-end">
              <Grid item>
                <Link component={RouterLink} variant="body2" to="/register">
                  Don't have an account?
                </Link>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </Container>
  );
};

export default Login;