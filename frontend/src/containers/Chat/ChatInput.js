import React, {useState} from 'react';
import {useSelector} from "react-redux";
import {Button, Grid, makeStyles, TextField} from "@material-ui/core";


const useStyles = makeStyles({
  button: {
    marginLeft: "20px",
  }
});

const ChatInput = ({ws}) => {
  const classes = useStyles();
  const userID = useSelector(state => state.users.user?._id);
  const [message, setMessage] = useState("");

  const onInputChange = event => {
    setMessage(event.target.value);
  };

  const sendButtonHandler = () => {
    ws.current.send(JSON.stringify({type: "ADD_MESSAGE", message, user: userID}));
    setMessage("");
  };

  return (
      <>
        <Grid item xs>
          <TextField
              variant="outlined"
              value={message}
              size="small"
              onChange={onInputChange}
          />
        </Grid>
        <Grid item className={classes.button}>
          <Button
              variant="contained"
              color="primary"
              onClick={sendButtonHandler}
          >
            Send
          </Button>
        </Grid>
      </>
  );
};

export default ChatInput;