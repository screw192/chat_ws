import React from 'react';
import {makeStyles, Typography} from "@material-ui/core";
import {nanoid} from "nanoid";


const useStyles = makeStyles({
  title: {
    position: "absolute",
    top: "-30px",
    left: "10px",
  }
});

const ChatUsers = ({users}) => {
  const classes = useStyles();
  const usersList = users.map(user => {
    return (
      <Typography variant="body1" key={nanoid()}>
        {user}
      </Typography>
    );
  });

  return (
      <>
        <Typography variant="h6" className={classes.title}>Online</Typography>
        {usersList}
      </>
  );
};

export default ChatUsers;