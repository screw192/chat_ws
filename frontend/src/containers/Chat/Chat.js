import React, {useEffect, useRef} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Grid, makeStyles, Typography} from "@material-ui/core";

import ChatUsers from "./ChatUsers";
import ChatMessages from "./ChatMessages";
import ChatInput from "./ChatInput";
import {websocketAction} from "../../store/actions/chatActions";


const useStyles = makeStyles({
  chatBlock: {
    marginTop: "40px",
    height: "80vh",
  },
  chatUsers: {
    border: "2px solid #cccccc",
    borderRadius: "5px",
    marginRight: "20px",
    padding: "15px",
    position: "relative",
  },
  chatMessages: {
    border: "2px solid #cccccc",
    borderRadius: "5px",
    marginBottom: "20px",
    padding: "15px",
    flexGrow: 1,
    position: "relative",
  }
});

const Chat = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);
  const userToken = useSelector(state => state.users.user?.token);
  const chatData = useSelector(state => state.chat);

  const ws = useRef(null);

  useEffect(() => {
    if (userToken) {
      ws.current = new WebSocket(`ws://localhost:8000/chat?token=${userToken}`);

      ws.current.onmessage = event => {
        const decoded = JSON.parse(event.data);

        dispatch(websocketAction(decoded));
      };
    }
  }, [dispatch, userToken]);

  return (
      user ? (
          <Grid container className={classes.chatBlock}>
            <Grid item xs={3} className={classes.chatUsers}>
              <ChatUsers
                  users={chatData?.users}
              />
            </Grid>
            <Grid item xs container direction="column">
              <Grid item className={classes.chatMessages}>
                <ChatMessages
                    messages={chatData?.messages}
                />
              </Grid>
              <Grid item container alignItems="center">
                <ChatInput
                    ws={ws}
                />
              </Grid>
            </Grid>
          </Grid>
      ) : (
          <Typography variant="h4" align="center">Register or login to see chat</Typography>
      )
  )
};

export default Chat;