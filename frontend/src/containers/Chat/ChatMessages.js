import React from 'react';
import {makeStyles, Typography} from "@material-ui/core";


const useStyles = makeStyles({
  title: {
    position: "absolute",
    top: "-30px",
    left: "10px",
  }
});

const ChatMessages = ({messages}) => {
  const classes = useStyles();
  const messagesList = messages.map(msg => {
    return (
        <Typography variant="body2" key={msg._id}>
          {msg.user.displayName}: {msg.message}
        </Typography>
    );
  });

  return (
      <>
        <Typography variant="h6" className={classes.title}>Chat</Typography>
        {messagesList}
      </>
  );
};

export default ChatMessages;