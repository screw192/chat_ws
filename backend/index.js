const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const users = require("./app/users");
const Message = require("./models/Message");
const exitHook = require('async-exit-hook');
const config = require('./config');
const User = require("./models/User");


const app = express();
app.use(express.json());
app.use(cors());

require("express-ws")(app);

const port = 8000;

app.use("/users", users);

const activeConnections = {};

app.ws("/chat", async (ws, req) => {
  const userToken = req.query.token;

  activeConnections[userToken] = ws;
  activeConnections[userToken].user = await User.findOne({token: userToken});

  Object.keys(activeConnections).forEach(key => {
    if (key === userToken) {
      return;
    }

    const connection = activeConnections[key];
    connection.send(JSON.stringify({
      type: "USER_CONNECTED",
      user: activeConnections[userToken].user.displayName,
    }));
  });

  const messages = await Message.find().sort({'datetime': -1}).limit(30).populate("user", "displayName");
  const users = Object.keys(activeConnections).map(key => {
    return activeConnections[key].user.displayName;
  });

  ws.send(JSON.stringify({
    type: "CONNECTED",
    messages,
    users,
  }));

  ws.on("message", async msg => {
    const decoded = JSON.parse(msg);

    if (decoded.type === "ADD_MESSAGE") {
      const messageData = {
        message: decoded.message,
        user: decoded.user,
        datetime: new Date(),
      };

      const message = new Message(messageData);
      await message.save();

      const newMessageID = message._id;
      const newMessage = await Message.findOne({_id: newMessageID}).populate("user", "displayName");

      Object.keys(activeConnections).forEach(key => {
        const connection = activeConnections[key];
        connection.send(JSON.stringify({
          type: "NEW_MESSAGE",
          newMessage: newMessage,
        }));
      });
    }
  });

  ws.on("close", () => {
    const userDisplayName = activeConnections[userToken].user.displayName;
    delete activeConnections[userToken];

    Object.keys(activeConnections).forEach(key => {
      const connection = activeConnections[key];
      connection.send(JSON.stringify({
        type: "USER_DISCONNECTED",
        user: userDisplayName,
      }));
    });
  });
});

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });

  exitHook(async callback => {
    await mongoose.disconnect();
    console.log('mongoose disconnected');
    callback();
  });
}

run().catch(console.error);