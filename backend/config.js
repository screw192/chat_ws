module.exports = {
  db: {
    url: 'mongodb://localhost/chat_websockets',
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    },
  },
};