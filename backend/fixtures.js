const mongoose = require('mongoose');
const config = require('./config');
const User = require("./models/User");
const Message = require("./models/Message");
const {nanoid} = require('nanoid');

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [abu, yoba, batya] = await User.create({
    email: 'abu@2ch.ru',
    password: 'abu',
    token: nanoid(),
    role: 'user',
    displayName: 'Abu Makaka'
  }, {
    email: 'yoba@2ch.ru',
    password: 'yoba',
    token: nanoid(),
    role: 'user',
    displayName: 'Yoba Yobich'
  }, {
    email: 'batya@2ch.ru',
    password: 'batya',
    token: nanoid(),
    role: 'user',
    displayName: 'Batya'
  });

  await Message.create({
    message: "Test 1",
    datetime: "2021-05-10T04:58:07.336Z",
    user: abu,
  }, {
    message: "Test 2",
    datetime: "2021-05-10T05:08:07.336Z",
    user: yoba,
  }, {
    message: "Test 3",
    datetime: "2021-05-10T05:18:07.336Z",
    user: abu,
  }, {
    message: "Test 4",
    datetime: "2021-05-10T05:28:07.336Z",
    user: batya,
  }, {
    message: "Test 5",
    datetime: "2021-05-10T05:38:07.336Z",
    user: yoba,
  }, {
    message: "Test 6",
    datetime: "2021-05-10T05:48:07.336Z",
    user: abu,
  }, {
    message: "Test 7",
    datetime: "2021-05-10T05:58:07.336Z",
    user: batya,
  }, {
    message: "Test 8",
    datetime: "2021-05-10T06:08:07.336Z",
    user: abu,
  }, {
    message: "Test 9",
    datetime: "2021-05-10T06:18:07.336Z",
    user: yoba,
  }, {
    message: "Test 10",
    datetime: "2021-05-10T06:28:07.336Z",
    user: abu,
  });

  await mongoose.connection.close();
};

run().catch(console.error);